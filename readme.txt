
1. Install setup virtual environment
    python3 -m venv venv
    . venv/bin/activate

2. Install modules
    pip -r requirements.txt

3. Run server
    export FLASK_APP=application.py
    export FLASK_ENV=development
    export FLASK_DEBUG=1
    flask run --host=0.0.0.0 --port 5000







CREATE TABLE IF NOT EXISTS application_users(
  id integer PRIMARY KEY,
  client_id text,
  username text,
  password text,
  device_id text
);
